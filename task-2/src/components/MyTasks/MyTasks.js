import React, { PureComponent } from 'react';
import Task from '../Task/Task';

class MyTasks extends PureComponent {
  render(){
  return (
    <div>
      {this.props.tasks ? Object.values(this.props.tasks).map((task, index) => <Task redy={task.status} remove={()=>this.props.remove(Object.keys(this.props.tasks)[index])}
        textTask={task.textTask}
        key={index} />) : <p>Заданий нет</p>}
    </div>
    );
  };
};

export default MyTasks;
