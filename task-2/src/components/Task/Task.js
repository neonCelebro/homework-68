import React from 'react';
import './Task.css';

const Task = (props) =>

  <div className="taskBody">
    <span className="taskMessage">{props.textTask}</span>
    <button onClick={props.remove} id="deletMessage" type="button" name="button"><i className="fa fa-trash" aria-hidden="true"></i></button>
       <span>{props.redy ? 'Готово' : ''}</span>
  </div>

export default Task;
