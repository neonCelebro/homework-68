import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://lesson-68-1cfb4.firebaseio.com/' // Your URL here!
});

export default instance;
