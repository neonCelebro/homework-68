const initialState = {
  tasks : null,
  loading: false,
  error: false,
}

const reducer = (state = initialState, action) =>{
  switch (action.type) {
    case 'FETCH_TASK_SUCCSES':
    return  {
       ...state,
       tasks: action.value,
       loading: false
     }
     case 'FETCH_TASK_REQUEST':
       return {
         ...state,
         loading: true
       }
      case 'FETCH_TASK_ERROR':
        return {
          ...state,
          error: true,
          loading: false
        }
        break;
    default: return state;

  }
};

export default reducer;
