import axios from '../axios-info';

const FETCH_TASK_REQUEST =  'FETCH_TASK_REQUEST';
const FETCH_TASK_SUCCSES = 'FETCH_TASK_SUCCSES';
const FETCH_TASK_ERROR = 'FETCH_TASK_ERROR';

export const fetchTaskRequest = () => {
  return {type: FETCH_TASK_REQUEST};
};
export const fetchTaskSucsess = value => {
  return {type: FETCH_TASK_SUCCSES, value};
};
export const fetchTaskError = () => {
  return {type: FETCH_TASK_ERROR};
};

export const fetchTask = () => {
  return dispatch => {
    dispatch(fetchTaskRequest());
    axios.get('/tasks.json').then(resp => {
      dispatch(fetchTaskSucsess(resp.data));
    }, error => {
      dispatch(fetchTaskError(error));
    });
    }
  };

export const fetchAddTaskHendler = (textTask) => {
  return dispatch => {
        dispatch(fetchTaskRequest());
    const newTask = {
        textTask,
        id: Date.now(),
        status: false,
        };
    axios.post('/tasks.json', newTask).finally(() => dispatch(fetchTask()));
  }
};

export const fetchRemoveTaskHendler = (id) => {
  return dispatch => {
        dispatch(fetchTaskRequest());
    axios.delete('/tasks/'+id +'.json').finally(() => dispatch(fetchTask()));
  }
};
