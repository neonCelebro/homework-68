import React, { Component } from 'react';
import  {connect} from 'react-redux';
import AddTaskForm from '../components/AddTaskForm/AddTaskForm.js';
import MyTasks from '../components/MyTasks/MyTasks';
import axios from '../axios-info';
import Spinner from '../components/UI/Spinner/Spinner';
import * as actions from '../store/actions';

class Tasks extends Component {
  state = {
    currentTask: 'New Task',
  };

  changeTaskText = (event) => this.setState({currentTask: event.target.value});

  clearTaskText = () => this.setState({currentTask: ''});

  returnText = () => this.setState({currentTask: 'New Task'});

  removeTaskHandler = (id) => {
      axios.delete('/tasks/'+id +'.json');
    };
    addNewTaskHandler = (textTask) =>{
        this.props.fetchAddTask(textTask);
        this.returnText();
    };

    componentDidMount(){
      this.props.fetchTask();
    };

    render() {
      if (this.props.loading) {
      return <Spinner />;
    }
    if (this.props.error) {
      return <div className='error'>Произошла ошибка, но нам лень навешивать логику скрытия этого сообщения, поэтому просто перезагрузите страницу</div>
    }
    return (
      <div>
        <AddTaskForm
          currentTask={this.state.currentTask}
          changeTask={this.changeTaskText}
          focusTask={this.clearTaskText}
          addTask={this.addNewTaskHandler}/>
        <MyTasks
          onServer={this.getInfoOnServer}
          tasks={this.props.tasks}
          remove={this.props.fetchRemoveTask}
          status={this.redyTask}
        />
      </div>
    );
  }
}

const mapStoretoProps = state =>{
  return {
    tasks: state.tasks,
    loading: state.loading,
    error: state.error,
  }
};
const mapDispatchToProps = dispatch => {
  return {
    fetchTask: () => dispatch(actions.fetchTask()),
    fetchAddTask:(text) => dispatch(actions.fetchAddTaskHendler(text)),
    fetchRemoveTask:(id) => dispatch(actions.fetchRemoveTaskHendler(id)),
  }
};

export default connect(mapStoretoProps, mapDispatchToProps)(Tasks);
