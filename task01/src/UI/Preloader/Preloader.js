import React from 'react';
import './Preloader.css';
import Loader from '../../assets/img/139.gif';

const Preloader = (props) => {
    return (
        <div className="loader">
            <img src={Loader} alt="loader"/>
        </div>
    )
};

export default Preloader;