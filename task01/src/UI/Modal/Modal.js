import React from 'react';
import './Modal.css';

const Modal = props => (
        <div
            className="Modal"
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
            }}
        >
            Something wrong happened
        </div>
);

export default Modal;