import { FETCH_COUNTER_REQUEST, FETCH_COUNTER_SUCCESS, FETCH_COUNTER_ERROR } from './actions';

const initialState = {
    counter: 0,
    isLoading: false,
    error: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {

        case FETCH_COUNTER_REQUEST:
            return {...state, isLoading: true};

        case FETCH_COUNTER_SUCCESS:
            return {...state, counter: action.counter, isLoading: false};

        case FETCH_COUNTER_ERROR:
            return {...state, error: true};

        default:
            return state;
    }
    return state;
};


export default reducer;

