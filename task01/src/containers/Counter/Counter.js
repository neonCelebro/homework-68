import React, { Component, Fragment } from 'react';
import './Counter.css';
import { connect } from 'react-redux';
import { fetchCounter, changeCounter } from "../../store/actions";
import Modal from '../../UI/Modal/Modal';
import Preloader from '../../UI/Preloader/Preloader';


class Counter extends Component {

    componentDidMount() {
        this.props.fetchCounter();
    }

    render() {

        if(this.props.isLoading) {
            return <Preloader show={this.props.isLoading}/>
        }

        return (
            <Fragment>
                <Modal show={this.props.error}/>
                <div className="Counter">
                    <h1>{!this.props.ctr ? null : this.props.ctr}</h1>
                    <button onClick={this.props.increaseCounter}>Increment</button>
                    <button onClick={this.props.decreaseCounter}>Decrement</button>
                    <button onClick={this.props.addCounter}>Increase by 5</button>
                    <button onClick={this.props.subtractCounter}>Decrease by 5</button>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter,
        error: state.error,
        isLoading: state.isLoading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        increaseCounter: () => dispatch(changeCounter(1)),
        decreaseCounter: () => dispatch(changeCounter(-1)),
        addCounter: () => dispatch(changeCounter(5)),
        subtractCounter: () => dispatch(changeCounter(-5)),
        fetchCounter: () => dispatch(fetchCounter()),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Counter);
